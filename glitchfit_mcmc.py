#!/usr/bin/env python

# Import functions
from __future__ import division
from __future__ import print_function

import numpy as np
import glob
import matplotlib.pyplot as plt
import scipy.linalg as sl
import scipy.stats as stats
import corner
import argparse
import sys
import os

from mpi4py import MPI
import enterprise
from enterprise.pulsar import Pulsar
import enterprise.signals.parameter as parameter
from enterprise.signals import utils
from enterprise.signals import signal_base
from enterprise.signals import selections
from enterprise.signals.selections import Selection
from enterprise.signals import white_signals
from enterprise.signals import gp_signals
from enterprise.signals import deterministic_signals
from enterprise_extensions import models, model_utils
from PTMCMCSampler.PTMCMCSampler import PTSampler as ptmcmc
import enterprise.constants as const

class measureGlitch(object):

    def __init__(self, ephemeris, timfile, priors, nsamp, disp, size, rank):

        # Set class variables
        self.ephemeris      = ephemeris      # Par file/Ephemeris
        self.timfile        = timfile        # File of pulse arrival times
        self.size           = size           # Number of system copies
        self.rank           = rank           # Energy level names
        self.priors         = priors         # List of priors
        self.nsamp          = int(nsamp)     # Chain length
        self.disp           = disp           # Flag to fit power-law DM


    @staticmethod
    def check_file_exists(this_file):
       if not os.path.isfile(this_file):
           print("File {} not found - exiting".format(this_file))
           sys.exit(9)

    def get_pulsar_frequency(self, eph):
        with open(eph, 'r') as this_eph:
            for line in this_eph.readlines():
                fields = line.split()
                if fields[0] == "F0":
                    f0 = float(fields[1])

        return f0

    @signal_base.function
    def glitch_function(toas, f0, GLEP_1, GLF0_1, GLF1_1, GLF2_1, GLF0D_1, GLF0D_2, GLF0D_3, GLTD_1, GLTD_2, GLTD_3):
        GLF0D_1 *= 1e-6
        GLF0D_2 *= 1e-6
        GLF0D_3 *= 1e-6
        GLF0_1 *= 1e-6
        GLF1_1 *= 1e-12
        GLF2_1 *= 1e-20
        t = toas/86400.0-GLEP_1

        jump = np.zeros_like(t)
        jump[t>=0] = -86400.0*GLF0_1*t[t>=0] - 0.5*GLF1_1*(86400.0*t[t>=0])**2.0 - (1.0/6.0)*GLF2_1*t[t>=0]**3.0

        expf = np.zeros_like(t)+1.0
        expf[t>=0] = np.exp(-t[t>=0]/GLTD_1)

        expf2 = np.zeros_like(t)+1.0
        expf2[t>=0] = np.exp(-t[t>=0]/GLTD_2)

        expf3 = np.zeros_like(t)+1.0
        expf3[t>=0] = np.exp(-t[t>=0]/GLTD_3)

        phs  = jump - GLF0D_1 * GLTD_1 * 86400.0 * (1.0-expf) - GLF0D_2 * GLTD_2 * 86400.0 * (1.0-expf2) - GLF0D_3 * GLTD_3 * 86400.0 * (1.0-expf3)

        return phs/f0

    def read_priors(self, priors_file):
        prior_dict = {}
        with open(priors_file, 'r') as priors:
            for line in priors.readlines():
                fields = line.split()
                if fields[0] == "GLEP_1":
                    prior_dict["GLEP_1"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "GLF0_1":
                    prior_dict["GLF0_1"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "GLF1_1":
                    prior_dict["GLF1_1"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "GLF2_1":
                    prior_dict["GLF2_1"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "GLF0D_1":
                    prior_dict["GLF0D_1"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "GLTD_1":
                    prior_dict["GLTD_1"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "GLF0D_2":
                    prior_dict["GLF0D_2"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "GLTD_2":
                    prior_dict["GLTD_2"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "GLF0D_3":
                    prior_dict["GLF0D_3"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "GLTD_3":
                    prior_dict["GLTD_3"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "EFAC":
                    prior_dict["EFAC"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "LOG10_A":
                    prior_dict["LOG10_A"] = [float(fields[1]), float(fields[2])]
                if fields[0] == "GAMMA":
                    prior_dict["GAMMA"] = [float(fields[1]), float(fields[2])]
 
        return prior_dict

    def set_rn_model(self, priors):
        if priors["LOG10_A"][0] == 0 and priors["LOG10_A"][1] == 0:
            print("Red noise parameter not set")
            return False
        if priors["GAMMA"][0] == 0 and priors["GAMMA"][1] == 0:
            print("Red noise parameter not set")
            return False
        ncos = 100
        Tspan=self.psr.toas.max()-self.psr.toas.min()
        log10_A = parameter.LinearExp(priors["LOG10_A"][0], priors["LOG10_A"][1]) 
        gamma = parameter.Uniform(priors["GAMMA"][0], priors["GAMMA"][1])
        pl = utils.powerlaw(log10_A=log10_A, gamma=gamma)
        rn = gp_signals.FourierBasisGP(spectrum=pl, components=ncos, Tspan=Tspan)

        return rn

    def set_white_model(self, priors):
        efac = parameter.Uniform(priors["EFAC"][0], priors["EFAC"][1])
        ef = white_signals.MeasurementNoise(efac=efac)
        return ef

    @signal_base.function
    def powerlaw_nogw(f, log10_A=-16, gamma=5):
        df = np.diff(np.concatenate((np.array([0]), f[::2])))
        return ((10**log10_A)**2 * const.fyr**(gamma-3) * f**(-gamma) * np.repeat(df, 2))

    @staticmethod
    def FourierBasisGP_DM(spectrum, components=20,
                   selection=Selection(selections.no_selection),
                   Tspan=None, name=''):
        """Convenience function to return a BasisGP class with a
        fourier basis."""

        basis = utils.createfourierdesignmatrix_dm(nmodes=components, Tspan=Tspan)
        BaseClass = gp_signals.BasisGP(spectrum, basis, selection=selection, name=name)

        class FourierBasisGP_DM(BaseClass):
            signal_type = 'basis'
            signal_name = 'dm noise'
            signal_id = 'dm_noise_' + name if name else 'dm_noise'

        return FourierBasisGP_DM

    def set_dm_model(self):
        if self.disp:
            log10_Adm = parameter.LinearExp(-15,-2)('DM_A')
            gamma_dm = parameter.Uniform(0,6)('DM_gamma')
            pldm = self.powerlaw_nogw(log10_A=log10_Adm, gamma=gamma_dm)
            nC = 60
            Tspan=self.psr.toas.max()-self.psr.toas.min()
            dm = self.FourierBasisGP_DM(spectrum=pldm, components=nC, Tspan=Tspan) 
            return dm
        else:
            return False
     
    def set_glitch_model(self, priors):

        # Set priors for epoch
        GLEP_1 = parameter.Uniform(priors["GLEP_1"][0], priors["GLEP_1"][1])
 
        # Set priors for GLF0
        if priors["GLF0_1"][0] == 0 and priors["GLF0_1"][1] == 0:
            GLF0_1 = parameter.Constant(0)
            print("Not fitting for GLF0")
        else:
            GLF0_1 = parameter.Uniform(priors["GLF0_1"][0], priors["GLF0_1"][1])

        # Set priors for GLF1
        if priors["GLF1_1"][0] == 0 and priors["GLF1_1"][1] == 0:
            GLF1_1 = parameter.Constant(0)
            print("Not fitting for GLF1")
        else:
            GLF1_1 = parameter.Uniform(priors["GLF1_1"][0], priors["GLF1_1"][1])

        # Set priors for GLF2
        if priors["GLF2_1"][0] == 0 and priors["GLF2_1"][1] == 0:
            GLF2_1 = parameter.Constant(0)
            print("Not fitting for GLF2")
        else:
            GLF2_1 = parameter.Uniform(priors["GLF2_1"][0], priors["GLF2_1"][1])

        # Set priors for first recovery component
        if priors["GLF0D_1"][0] == 0 and priors["GLF0D_1"][1] == 0:
            GLF0D_1 = parameter.Constant(0)
            GLTD_1 = parameter.Constant(1)
            print("Not fitting for exponential 1")
        else:
            GLF0D_1 = parameter.Uniform(priors["GLF0D_1"][0], priors["GLF0D_1"][1])
            GLTD_1 = parameter.Uniform(priors["GLTD_1"][0], priors["GLTD_1"][1])

        # Set priors for second recovery component
        if priors["GLF0D_2"][0] == 0 and  priors["GLF0D_2"][1] == 0:
            GLF0D_2 = parameter.Constant(0)
            GLTD_2 = parameter.Constant(1)
            print("Not fitting for exponential 2")
        else:
            GLF0D_2 = parameter.Uniform(priors["GLF0D_2"][0], priors["GLF0D_2"][1])
            GLTD_2 = parameter.Uniform(priors["GLTD_2"][0], priors["GLTD_2"][1])

        # Set priors for third recovery component
        if priors["GLF0D_3"][0] == 0 and priors["GLF0D_3"][1] == 0:
            GLF0D_3 = parameter.Constant(0)
            GLTD_3 = parameter.Constant(1)
            print("Not fitting for exponential 3")
        else:
            GLF0D_3 = parameter.Uniform(priors["GLF0D_3"][0], priors["GLF0D_3"][1])
            GLTD_3 = parameter.Uniform(priors["GLTD_3"][0], priors["GLTD_3"][1])

        glitch =  self.glitch_function(GLEP_1=GLEP_1, 
                                       GLF0D_1=GLF0D_1, GLTD_1=GLTD_1, 
                                       GLF0_1=GLF0_1, GLF1_1=GLF1_1, 
                                       GLF2_1=GLF2_1, 
                                       GLF0D_2=GLF0D_2, GLTD_2=GLTD_2, 
                                       GLF0D_3=GLF0D_3, GLTD_3=GLTD_3)

        glitch_f = deterministic_signals.Deterministic(glitch, name='gl')

        return glitch_f

    def setup_models(self, priorslist):
        models = []
        i = 1
        for item in priorslist:

            print("\nSetting up model {}".format(i))

            # Get dictionary of priors
            priors = self.read_priors(item)

            # Set up least squares timing model
            tm = gp_signals.TimingModel()

            # Set up white noise model
            white = self.set_white_model(priors) 

            # Set up red noise model
            rn = self.set_rn_model(priors)

            # Set up glitch model
            glitch = self.set_glitch_model(priors)

            # Set up DM model
            dmmodel = self.set_dm_model()

            if dmmodel:
                print("Fitting for power-law DM")
                # Construct global model
                if rn:
                    model = tm + white + glitch + rn + dmmodel
                else:
                    model = tm + white + glitch + dmmodel

            else:
                print("Not fitting for power-law DM")
                # Construct global model
                if rn:
                    model = tm + white + glitch + rn
                else:
                    model = tm + white + glitch

            models.append(model)

            i+=1

        return models

            
    def run(self):

        # Check ephemeris provided actually exists
        self.check_file_exists(self.ephemeris)
        
        # Check tim file provided actually exists
        self.check_file_exists(self.timfile) 

        # Create pulsar object
        self.psr = Pulsar(self.ephemeris, self.timfile)

        # Extract spin of pulsar from par file
        f0 = self.get_pulsar_frequency(self.ephemeris)
        self.psr.f0 = f0
        
        print("Least squares only for {}".format(self.psr.fitpars))

        models = self.setup_models(self.priors)

        # Prepare for sampling
        nmodels = len(models)
        mod_index = np.arange(nmodels)
        pta = dict.fromkeys(mod_index)

        if nmodels == 1:
            pta[0] = signal_base.PTA(models[0](self.psr))
        elif nmodels == 2:
            pta[0] = signal_base.PTA(models[0](self.psr))
            pta[1] = signal_base.PTA(models[1](self.psr))
        else:
            print("Not set up for > 2 models yet!")
            sys.exit(9)
      
        super_model = model_utils.HyperModel(pta)
 
        sampler = super_model.setup_sampler(resume=False, outdir='chains/'+self.psr.name+'/')

        # Set initial sample values
        x0 = super_model.initial_sample()
         
        # Dimension of parameter space
        ndim = len(x0)
 
        # Begin sampling
        sampler.sample(x0, self.nsamp, SCAMweight=30, AMweight=15, DEweight=50)

def main():

    parser = argparse.ArgumentParser(description='Markov chain Monte Carlo glitch measurement module')
    parser.add_argument('-e','--ephemeris', help='Pular Ephemeris', required=True)
    parser.add_argument('-t','--timfile', help='File of TOAs (TEMPO2 format)', required=True)
    parser.add_argument('-p', '--priors', nargs='+', help='List of priors', required=True)
    parser.add_argument('-n', '--nsamp', help='Number of samples', required=False, default=1e6, type=int)
    parser.add_argument('-d', '--disp', help='Fit power-law DM', action='store_true')
    args = parser.parse_args()

    if len(args.priors) > 2:
        print("Can only handle two models, exiting")
        sys.exit(9)

    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    measure = measureGlitch(args.ephemeris, args.timfile, args.priors, args.nsamp, args.disp, size, rank)
    measure.run()

if __name__ == '__main__':
    main()
